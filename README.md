To jest szablon zadania rekrutacyjnego Unity-t. Nie musisz się ograniczać do niego, jednakże może on przyspieszyć stworzenie rozwiązania. Możesz zmienić język programowania z Java na dwolony język programowania działający na JVM (jednakże preferujemy Java lub Kotlin).

Jeżeli skorzystasz z tego szablonu to prosimy o zastosowanie się do poniższych instrukcji:

1. Stwórz **prywatny** fork tego repozytorium https://bitbucket.org/unityttech/recruitment/fork
2. Utwórz nową gałąź i użyj jej dodając swoje zmiany.
3. Utwórz pull request z gałęzi utworzonej w punkcie 2. do gałęzi `master` z **TWOJEGO REPOZYTORIUM**.
4. Dodaj nam dostęp do zapisu do Twojego repozytorium. Szczegółowe informacje znajdziesz w pliku PDF, który od nas otrzymałeś.
